/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include <stdio.h>

#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
/* Example application name and version to display on LCD screen. */

struct time_timestamp tx_node[MAX_TARGET_NODE];
static unsigned char distance_seqnum = 0;
static srd_msg_dsss *msg_f_recv ;
/******************************************************************************************************************
********************* NOTES on DW (MP) features/options ***********************************************************
*******************************************************************************************************************/
#define BroadCastOverTime 10 //over time 10ms,normally measure ranging time about 5ms
uint32 TimeBroadCastComplete = 0;
uint8 BroadCastComplete = 0;

/* Private functions ---------------------------------------------------------*/
void Tx_Simple_Rx_Callback()
{
    int i=0;
    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    /* Activate reception immediately. See NOTE 2 below. */
    dwt_enableframefilter(DWT_FF_RSVD_EN);//disable recevie
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);

    if (status_reg & SYS_STATUS_RXFCG)
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        if (frame_len <= FRAME_LEN_MAX)
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);
            msg_f_recv = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f_recv->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f_recv->sourceAddr[1];

            msg_f_send.seqNum = msg_f_recv->seqNum;

            switch(msg_f_recv->messageData[0])
            {
                case 'd'://distance
                    BroadCastComplete =1;
                    tx_node[msg_f_recv->messageData[1]].tx_ts[0] = get_tx_timestamp_u64();
                    tx_node[msg_f_recv->messageData[1]].rx_ts[0] = get_rx_timestamp_u64();
                    break;
                default:
                    break;
            }

        }
    }
    else
    {
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        //enable recive again
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_setdelayedtrxtime(dwt_readsystimestamphi32() + 0x100000/2);
        dwt_rxenable(0);
    }
}

void BPhero_Distance_Measure()
{
    uint8 index = 0;
    uint16 RX_Address = 0x0001;
    {
        msg_f_send.destAddr[0] = RX_Address&0xFF;
        msg_f_send.destAddr[1] = (RX_Address>>8) &0xFF;
      
        final_msg_set_ts(&msg_f_send.messageData[FIRST_TX],  tx_node[index].tx_ts[0] );
        final_msg_set_ts(&msg_f_send.messageData[FIRST_RX],  tx_node[index].rx_ts[0] );

        msg_f_send.seqNum = distance_seqnum;
        msg_f_send.messageData[0]='D';
        msg_f_send.messageData[1]=index;

        dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
        dwt_writetxfctrl(psduLength, 0);
		  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
        dwt_starttx(DWT_START_TX_IMMEDIATE);

        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_rxenable(0);
 
        //wait 10ms
        TimeBroadCastComplete = portGetTickCnt();
        BroadCastComplete = 0;
        while((BroadCastComplete ==0)&&(portGetTickCnt()<(TimeBroadCastComplete+BroadCastOverTime)));
        dwt_forcetrxoff();
				 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
        /* Clear good RX frame event in the DW1000 status register. */
        if(++distance_seqnum == 255)
            distance_seqnum = 0;
    }
}

int tx_main(void)
{
//    char lcd_display_str[100]= {'0'};
//    sprintf(lcd_display_str, "UWB-TX:0x%04X", SHORT_ADDR);
//    OLED_ShowString(0,0,(uint8_t *)lcd_display_str);

    bphero_setcallbacks(Tx_Simple_Rx_Callback);
    /* Infinite loop */
    while(1)
    {
        BPhero_Distance_Measure();
        deca_sleep(1);
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
